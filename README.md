# AWS CDK S3 Bucket Project

This project contains the source code and supporting files for deploying an S3 bucket with AWS Cloud Development Kit (AWS CDK). It includes the following files and folders:

- `bin/` - Contains the CDK app initialization script.
- `lib/` - Contains the stack definition script.
- `node_modules/` - Contains the npm packages upon which the project depends.
- `test/` - Contains the unit tests for the CDK application.

## Prerequisites

- Node.js (Version 18.x or 20.x as of the last update to this README)

    ```bash
    npm install aws-cdk-lib
    npm install -g typescript
    ```

    - if Node.js is not installed on your system:
    - for macOS:
            ```bash
                curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
                nvm install node
            ```
    
- AWS CLI configured with the user's credentials and default region
    - if AWS Command Line Interface (CLI) is not installed on your system:
        - Follow the instructions on the AWS CLI version 2 installation page.

    - aws configure sso
        - (aws identify center contains the aws access portal URL... )
        - (create new user, add permission)
    - aws sso login --profile your-profile-name
- AWS CDK Toolkit installed globally (`npm install -g aws-cdk`)


## Bootstrapping the Environment

If you're deploying the CDK app for the first time in an AWS environment (account/region), you need to run:

```bash
cdk bootstrap aws://ACCOUNT-NUMBER/REGION --profile your-profile-name
```

Replace `ACCOUNT-NUMBER` and `REGION` with your AWS account number and the desired AWS region, respectively.

## Setting Up Project

```bash
mkdir -p projectname
cd projectname
cdk init app --language typescript
```

## Deploying the Application

Compile the TypeScript to JavaScript before deployment:

```bash
npm run build
```

Deploy the CDK stack to your default AWS account and region:

```bash
cdk deploy
```

If you want to specify a different profile for deployment:

```bash
cdk deploy --profile your-profile-name
```

## Viewing the S3 Bucket

After deployment, the S3 bucket can be viewed in the AWS Management Console under the S3 service for the region where you deployed the stack.

## Destroying the Stack

When you no longer need the stack, you can delete it, which will also remove the S3 bucket created by the stack:

```bash
cdk destroy --profile your-profile-name
```

## Notes

- The `CDKToolkit` stack will be present as long as you are using AWS CDK within that AWS account and region. It is a toolkit stack that manages deployments and should not be deleted unless you are ceasing to use CDK entirely.
- If you encounter an issue where a stack has been destroyed but the S3 bucket persists, check the bucket's deletion policy and ensure no other dependencies prevent its removal.

Be sure to replace placeholders (like `[your name/alias]`, `ACCOUNT-NUMBER`, `REGION`, and `your-profile-name`) with actual data relevant to your project.

## Screen shots:
-  AWS CodeWhisperer for CDK Code Generation
    - ![Alt text](./images/0.jpg "Optional title0")

-  Deployment:
    - ![Alt text](./images/1.jpg "Optional title1")

- on aws:
    - ![Alt text](./images/2.jpg "Optional title2")

- bucket version
    - ![Alt text](./images/3.jpg "Optional title3")


