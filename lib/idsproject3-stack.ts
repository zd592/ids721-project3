import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib'; // ** the lib for s3
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class Idsproject3Stack extends cdk.Stack {
  // constructor(scope: Construct, id: string, props?: cdk.StackProps) {
  //   super(scope, id, props);

  //   // The code that defines your stack goes here

  //   // example resource
  //   // const queue = new sqs.Queue(this, 'Idsproject3Queue', {
  //   //   visibilityTimeout: cdk.Duration.seconds(300)
  //   // });
  // }

  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new s3.Bucket(this, 'idsBucket', {
      versioned: true,
      // encryption: s3.BucketEncryption.S3_MANAGED,
      encryption: s3.BucketEncryption.KMS_MANAGED
    });
  }
}
